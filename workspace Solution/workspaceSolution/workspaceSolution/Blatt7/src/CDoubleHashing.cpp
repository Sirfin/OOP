#include "CDoubleHashing.h"


CDoubleHashing::CDoubleHashing()
{
	// do nothing
}

// Der Konstruktor wird einmalig aufgerufen
CDoubleHashing CDoubleHashing::m_instance;

CDoubleHashing& CDoubleHashing::getInstance()
{
	return m_instance;
}

unsigned int CDoubleHashing::hash(unsigned int I, unsigned int J, unsigned int dictSize, unsigned int attempt)
{
	unsigned long I_l = static_cast<unsigned long>(I);
	unsigned long J_l = static_cast<unsigned long>(J);
	unsigned long hash_value = (I_l + J_l)*(I_l + J_l + 1)/2 + J_l;

	// berechne den Hashwert gem�� der angegebenen Funktion
	unsigned int h1 = hash_value % dictSize;
	unsigned int h2 = 1 + (hash_value % (dictSize - 2));
	unsigned int result = (h1 + attempt*h2) % dictSize;
	
	return result;

}



