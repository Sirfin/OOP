/************************************************************
 *                  Unit-Test f�r Blatt 7                   *
 ************************************************************/

// Bitte hier die aktuelle Aufgabe festlegen
#define AUFGABE_2

// Ben�tigte Bibliotheken einbinden
#include <iostream>

// Google Test einbinden
#include "gtest/gtest.h"

#include "CDoubleHashing.h"

TEST(CDoubleHashingTest, SimpleHashing) {
	CDoubleHashing& hash = CDoubleHashing::getInstance();
	EXPECT_EQ(4, hash.hash(3, 4, 7, 0));
	EXPECT_EQ(1, hash.hash(1, 2, 7, 0));
	EXPECT_EQ(0, hash.hash(2, 1, 7, 0));
}

TEST(CDoubleHashingTest, DoubleHashing) {
	CDoubleHashing& hash = CDoubleHashing::getInstance();
	EXPECT_EQ(10, hash.hash(3, 4, 11, 0));
	EXPECT_EQ(5, hash.hash(3, 4, 11, 1));
	EXPECT_EQ(0, hash.hash(3, 4, 11, 2));
	EXPECT_EQ(6, hash.hash(3, 4, 11, 3));
}

// Hauptprogramm
int main(int argc, char** argv) {
	// Google Test initialisieren
	testing::InitGoogleTest(&argc, argv);

	// alle Tests ausf�hren
	int ret = RUN_ALL_TESTS();

	// und Ergebnis anzeigen und zur�ckgeben
	std::cin.get();
	return ret;
}
