/****************************************************
*                   CDoubleHashing                  *
*****************************************************
* Entwurfsmuster: Singleton                         *
* Diese Klasse stellt den Algorithmus f�r doppeltes *
* Hashing bereit. Sie ist in Anlehnung an das       *
* Skript nach dem einfachsten Singletonmuster       *
* ohne Lazy-Loading und release-Methode             *
* implementiert                                     *
* Der Algorithmus vereint zun�chst die beiden zu    *
* hashenden Integer zu einer neuen Integerzahl.     *
* Es ist klar, dass zwei unterschiedliche Zahlen-   *
* paare m�glichst auch auf unterschiedliche Zahlen  *
* abgebildet werden sollten, damit die Hashfunktion *
* m�glichst wenige Kollisionen produziert.          *
* Dies geschieht mit der Cantor-Pairing-Funktion.   *
* Die doppelte Hashfunktion ist gegeben durch:      *
* h1(k)= k mod m (lineares Sondieren)               *
* h2(k)= 1 + k mod (m-2)                            *
* h(k)= (h1(k) + i*h2(k)) mod m                     *
* mit m = Groe�e des Dictionaries.                  *
*****************************************************/
#pragma once

class CDoubleHashing
{
public:
	// Gibt die einzige Instanz der Klasse zur�ck
	static CDoubleHashing& getInstance();

	// Berechnet den Hashwert zweier Integer I und J mittels
	// doppeltem Hashing f�r eine Hashtabelle der Gr��e dict_size
	unsigned int hash(unsigned int I, unsigned int J, unsigned int dict_size, unsigned int attempt);

private:
	// Konstruktor, private gem�� Singletonmuster
	CDoubleHashing();
	// Kopierkonstruktor verbieten gem�� Singletonmuster
	CDoubleHashing(const CDoubleHashing& other);
	// Zuweisungsoperator verbieten gem�� Singletonmuster
	CDoubleHashing operator=(CDoubleHashing& other);

	// die einzige Instanz
	static CDoubleHashing m_instance;
};

