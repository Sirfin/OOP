/***************************************************
*                    XOutOfBounds                  *
****************************************************
* Diese Klasse repräsentiert eine Ausnahme, die im *
* Falle eines ungültigen Indexes beim Dictionary   *
* geworfen wird.                                   *
****************************************************/
#pragma once

#include <exception>
#include <string>

class XOutOfBounds : public std::exception
{
	std::string msg;
public:
	XOutOfBounds(const char* msg);//Konstruktor, erwartet eine Fehlermeldung als Parameter
	virtual ~XOutOfBounds() throw();
	virtual const char* what() const throw();//gibt Fehlermeldung zurück
};
