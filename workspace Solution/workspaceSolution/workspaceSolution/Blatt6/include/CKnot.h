/****************************************************
*                       CKnot                       *
*****************************************************
* Diese Klasse repr�sentiert einen Knoten in der    *
* Trie-Implementierung des LZW.                     *
*****************************************************/
#pragma once

#include "CEntry.h"

class CKnot : public CEntry
{
public:
	// Konstruktor, parameterlos
	CKnot();
	
	// Getter f�r das Attribut Index
	int getIndex();
	// Setter f�r das Attribut Index
	void setIndex(int i);
	// Getter f�r das Attribut Parent
	int getParent();
	// Setter f�r das Attribut Parent
	void setParent(int p);

private:
	// Index(Hashwert) des Knoten
	int index;
	// Vaterknoten
	int parent;
};