/***************************************************
*                    CArray                   *
****************************************************
* Diese Templateklasse repr�sentiert ein           *
* intelligentes Array beliebigen Datentyps und     * 
* Gr��e, das eine Indexpr�fung vornehmen kann und  *
* im Falle eines ung�ltigen Zugriffs eine          *
* XOutOfBounds-Exception wirft.                    *
****************************************************
* Template-Parameter:                              *
*   T: Datentyp des Arrays                         *
*   N: Gr��e des Arrays                            *
****************************************************/
#pragma once

#include "XOutOfBounds.h"

template<typename T, unsigned int N> class CArray
{
public:
	// Konstruktor
	CArray();
	// Destruktor
	~CArray();
	// �berladen des []-Operators zum Zugriff auf Arrayelemente
	T& operator[](unsigned int index);

private:
	// Das eigentliche Datenarray
	T* m_entries;
};

template<typename T, unsigned int N> CArray<T,N>::CArray() : m_entries(new T[N]) {
}

template<typename T, unsigned int N> CArray<T,N>::~CArray() {
	if (m_entries != 0) {
		delete[] m_entries;
		m_entries = 0;
	}
}

template<typename T, unsigned int N> T& CArray<T,N>::operator[](unsigned int index) {
	// Indexpr�fung, wg. unsigned int keine Pr�fung auf negative Indexwerte notwendig
	if (index >= N)
		throw XOutOfBounds("Index zu gross");
	return m_entries[index];
}
