#include "CKnot.h"

// index und parent mit -1(unused) initialisiert
//CKnot::CKnot() : index(-1), parent(-1) {
//}

// index mit -1(unused) und parent mit 0(no parent/root) initialisiert
CKnot::CKnot() : index(-1), parent(0) {
}

int CKnot::getIndex() {
	return index;
}

void CKnot::setIndex(int i) {
	index = i;
}

int CKnot::getParent() {
	return parent;
}

void CKnot::setParent(int p) {
	parent = p;
}
