#include "CEntry.h"

// Zum Laufzeitbeginn wird die Anzahl aller Instanzen auf 0 gesetzt
unsigned int CEntry::anzahl = 0;

CEntry::CEntry() : symbol("") {
	// Erzeugen einer Instanz f�hrt zur Inkrementierung von anzahl
	anzahl++;
}

CEntry::~CEntry() {
	// L�schen einer Instanz f�hrt zur Dekrementierung von anzahl
	anzahl--;
}

string& CEntry::getSymbol() {
	return symbol;
}

void CEntry::setSymbol(string str) {
	symbol = str;
}

unsigned int CEntry::getNumber() {
	return anzahl;
}
