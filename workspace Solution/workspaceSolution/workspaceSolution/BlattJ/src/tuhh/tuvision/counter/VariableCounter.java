package tuhh.tuvision.counter;

public class VariableCounter extends BaseCounter {
	
	protected int stepSize;

	VariableCounter(){
		stepSize = 0;
	}
	
	VariableCounter(int stepSize){
		this.stepSize = stepSize;
	}
	
	@Override
	public void count() {
		value += stepSize;
	}

}
