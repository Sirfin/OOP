package tuhh.tuvision.counter;

public abstract class BaseCounter implements Counter{
	protected int value;
	
	public int getValue(){
		return value;
	}
	
	public void setValue(int value){
		this.value = value;
	}
	
	public void adoptValue(Counter counter){
		value = counter.getValue();
	}
}
