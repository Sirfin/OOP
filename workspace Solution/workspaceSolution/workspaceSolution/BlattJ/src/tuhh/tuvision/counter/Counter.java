package tuhh.tuvision.counter;

public interface Counter {
	void count();
	int getValue();
	void setValue(int value);
	void adoptValue(Counter counter);
}
