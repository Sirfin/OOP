/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : 01.10.2002
    copyright            : (C) 2002 by TUHH, Vision Systems
    email                : kricke@tu-harburg.de
    origin               : Informatik fuer Ingenieure II, WS 2002/03
	last modified		 : Rosenstiel 2008
 ***************************************************************************/

 /**************************************************************************
 Gegenstand dieses Hello-World Beispiels:
 #include <...>
 using namespace
 Ausgabe mit cout in ein DOS-Fenster
 Programmausführung anhalten um Ausgabetext sichtbar zu halten
 ***************************************************************************/
 
#include <iostream>     // stellt Ein- und Ausgaberoutinen zu Verfügung
#include <cstdlib>      // Dort ist EXIT_SUCCESS und die "system" - Funktion definiert
                        // Durch #include <...> wird im Standardpfad gesucht
                        // Bei #include "..." würde im lokalen Directory gesucht werden.

using namespace std;    // Namesraum std wird voreingestellt. 
                        // Ohne diese Zeile müsste es unten z.B. std::cout heissen.
                        
int main()              // das Hauptprogramm heisst immer main
{             
    cout << "Hello World!" << endl;
    cout << 4 << " ist eine tolle Zahl" << endl;
                        // endl beginnt eine neue Zeile.
  
    
    system("PAUSE");    // MS-DOS Befehl, hält Programmausführung an;
                        // sonst schließt das Ausgabefenster sofort
   
    return EXIT_SUCCESS;    
    

}
