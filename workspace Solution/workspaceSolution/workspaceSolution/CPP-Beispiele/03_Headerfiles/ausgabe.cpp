/***************************************************************************
                          ausgabe.cpp  -  description
                             -------------------
    begin                : 01.10.2002
    copyright            : (C) 2002 by TUHH, Vision Systems
    email                : kricke@tu-harburg.de
    origin               : Informatik fuer Ingenieure II, WS 2002/03

 ***************************************************************************/

#include "ausgabe.h"
                        // #include"...": Dateien werden 
                        //      im lokalen Verzeichnis gesucht 
#include <iostream>
                        // #include<...>: Dateien werden
                        //      im Systempfad gesucht 

using namespace std;

void ausgabe()
{

    cout << "Hello World!" << endl;
    cout << Meine_Tolle_Int_Zahl << " ist eine tolle Zahl" << endl;

}

