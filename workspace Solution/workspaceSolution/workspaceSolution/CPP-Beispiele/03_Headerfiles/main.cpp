/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : 01.10.2002
    copyright            : (C) 2002 by TUHH, Vision Systems
    email                : kricke@tu-harburg.de
    origin               : Informatik fuer Ingenieure II, WS 2002/03

 ***************************************************************************/

/***************************************************************************
 *                                                                          
 *   In diesem Programm wurde gegen�ber o2_FunktionDefineConst ausgelagert:                                  
 *   die Deklarationen, d.h. die Beschreibung der Schnittstelle,in das 
 *   sogenannte Header-File ausgabe.h;
 *   die Definition von ausgabe(), d.h. die Implementation, in das File ausgabe.cpp.
 *   Hierdurch wird das Programm f�r den Anwender �bersichtlicher. 
 *                                                                         *
 ***************************************************************************/


#include "ausgabe.h"  // Auch in ausgabe.cpp wird dieses include ausgef�hrt. 
                      // Nur durch das #define AUSGABE_H in ausgabe.h
                      // zusammen mit dem ifndef AUSGABE_H
                      // wird Doppelinitialisierung verhindert 

#include <iostream>   // ist hier in diesem Fall nicht zwingend, 
                      // aber auch nicht sch�dlich (s.o.)
#include <cstdlib>


using namespace std;


int main()
{

    ausgabe();
    
    system("PAUSE");
    
    return EXIT_SUCCESS;    
    

}
