/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : 01.10.2002
    copyright            : (C) 2002 by TUHH, Vision Systems
    email                : kricke@tu-harburg.de
    origin               : Informatik fuer Ingenieure II, WS 2002/03

 ***************************************************************************/

// Beispiel f�r Parameter�bergabe mittels Call By Reference


#include <iostream>
#include <cstdlib>

using namespace std;

void funktion(int& a)
{
                              // a wird mit dem Aufrufparameter initialisiert und
                              // ist innerhalb der Funktion ein anderer Name f�r den
                              // Aufrufparameter. Neuer Speicherplatz wird f�r a nicht 
                              // erzeugt. Die Referenz a lebt bis zum Ende des 
                              // Funktionsblocks.

    cout << "Die lokale Zahl a in funktion vor Zuweisung ist a=" << a << endl;
    a = 9;
    cout << "Die lokale Zahl a in funktion nach Zuweisung ist a=" << a << endl;
}

int main()
{

    int a = 5;
    
    cout << "Die Zahl a ist vor Aufruf von funktion a=" << a << endl;
    
    funktion(a);    // call by reference, vorgegeben durch int& in Definition von funktion
   
    cout << "Die Zahl a ist nach Ruecksprung aus funktion a=" << a << endl;
    
    system("PAUSE");
    
    return EXIT_SUCCESS;
    

}
