/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : 01.10.2002
    copyright            : (C) 2002 by TUHH, Vision Systems
    email                : wenzel@tu-harburg.de
    origin               : Informatik fuer Ingenieure II, WS 2002/03

 ***************************************************************************/

#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
  // Datentypen und sizeof - operator
  
  char a='a';
  
  cout << "Character a ist " << a << "; ein char    ist " << sizeof(a) << " Bytes lang" << endl;
  
  // Integer b wird nicht initialisiert, der Wert ist also undefiniert
  // (Der Wert, der dort vorher im Speicher war, nur im release-Modus)
  int b;
  
  cout << "Integer   b ist " << b << "; ein Integer ist " << sizeof(b) << " Bytes lang" << endl;
  
  float c=1.0;
  
  cout << "Float     c ist " << c << "; ein Float   ist " << sizeof(c) << " Bytes lang" << endl;
  
  double d=c;
  
  cout << "Double    d ist " << d << "; ein Double  ist " << sizeof(d) << " Bytes lang" << endl;
  
  
  // Hier kommen als Anwendungsbeispiel noch einige weitere Typen und zugehörige Literale:
  
  double mydouble = 1.23E-6;
  
  long mylong = 1234L;
  short myshort = 1234;
  
  unsigned short us = 1234U;
  unsigned int ui = 1234U;
  unsigned long ul = 1234UL;
  
  int myhex = 0XAD0FF;
  int myoctal = 01234;
  
  //short, int, long sind per default signed.
  //signed in signed short, signed int, signed long kann also entfallen
  
  cout << "Long     mylong   ist " << mylong <<       "; ein long  ist           " << sizeof(mylong) << " Bytes lang" << endl;
  cout << "Short    myshort  ist " << myshort <<      "; ein short  ist          " << sizeof(myshort) << " Bytes lang" << endl;
  cout << "Unsigned short us ist " << us <<           "; ein unsigned short  ist " << sizeof(us) << " Bytes lang" << endl;
  cout << "Unsigned int  ui  ist " << ui <<           "; ein unsigned int  ist   " << sizeof(ui) << " Bytes lang" << endl;
  cout << "Unsigned long ul  ist " << ul <<           "; ein unsigned long ist   " << sizeof(ul) << " Bytes lang" << endl;
  cout << "Hex      myhex    ist " << hex << myhex << "; ein hex ist             " << sizeof(d) << " Bytes lang" << endl;
  cout << "Octal    myoctal  ist " << myoctal <<      "; ein octal  ist          " << sizeof(d) << " Bytes lang" << endl;

  system("PAUSE");	
  return EXIT_SUCCESS;
}
