/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : 01.10.2002
    copyright            : (C) 2002 by TUHH, Vision Systems
    email                : kricke@tu-harburg.de
    origin               : Informatik fuer Ingenieure II, WS 2002/03

 ***************************************************************************/

// Beispiel für Parameterübergabe durch Übergabe der Adresse, d.h. durch Zeiger


#include <iostream>
#include <cstdlib>

using namespace std;

void funktion(int* b)    // Parameterübergabe durch Zeiger, der die Adresse 
                         // des Parameters enthält. 

{
    cout << "In funktion:  b zeigt auf die Adresse " << hex 
        << b << endl;

    cout << "In funktion:  Die Zahl ist " << *b << endl;
    *b = 9;
    cout << "In funktion:  Die Zahl ist " << *b << endl;
}

int main()
{
    int a = 5;
    
    cout << "Vor Aufruf von funktion: Die Zahl ist " << a << endl;
    
    cout << "Vor Aufruf von funktion: Die Adresse von a ist " << hex << &a << endl;
    
    funktion(&a);   // uebergebe Pointer
   
    cout << "Nach Rücksprung aus funktion:  Die Zahl ist " << a << endl;
    
    system("PAUSE");
    
    return EXIT_SUCCESS;
}
