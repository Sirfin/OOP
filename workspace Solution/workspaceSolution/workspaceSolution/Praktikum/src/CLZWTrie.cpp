#include "CLZWTrie.h"

CLZWTrie::CLZWTrie()
{
	// Initialisierung der ersten 256 Eintr�ge im Dictionary mit den 
	// entsprechenden Zeichen der Ascii-Tabelle
	for (int i = 0; i < 256; i++) {
		m_symbolTable[i].setSymbol(intToString(i));
		m_symbolTable[i].setParent(-1);
	}
	for (int i = 0; i < 256; i++) {
		m_symbolTableDec[i].setSymbol(intToString(i));
		m_symbolTableDec[i].setParent(-1);
	}
}

vector<unsigned int> CLZWTrie::encode(const std::string &in)
{
	// Output des Algorithmus
	vector<unsigned int> out;
	// parentPos: Position des Vaterknotens, im Dictionary vorhanden
	// symbolVal: Numerischer ASCII-Wert des n�chsten Zeichens des Eingabestrings
	unsigned int parentPos = 0;
	unsigned int symbolVal = 0;
	// Hashwert, bleibt w�hrend Rehashing unver�ndert
	unsigned int hashVal = 0;
	// Index, tempor�re Adresse w�hrend Hashing und Rehashing
	unsigned int pos = 0;
	// Counter f�r das Rehashing, initial 0
	CForwardCounter attemptCounter;
	attemptCounter.setValue(0); // Initialisierung

	// Iterator f�r Eingabestring
	string::const_iterator it = in.begin();
	// Iterator f�r Endabfrage
	string::const_iterator iend = in.end();

	// Beenden, falls Eingabestring zu Beginn leer
	if(it == iend) return out;
	
	// Erstes Zeichen blind �bernehmen, in seinen numerischen ASCII-Code wandeln
	// und diesen Wert als Position verwenden.	
	// Achtung, cast erforderlich f�r Werte >127, sonst erh�lt man negative oder sehr gro�e Zahlen
	parentPos = charToInt(*it); // Position des ersten Zeichens ist sein ASCII-Wert
	

	// �ber den Eingabestring iterieren
	while(1) // "Endlosscheife" �ber den Eingabestring in
		     // Schleife terminiert, da in jedem Durchlauf Iterator inkrementiert wird		                
	{
		// Iterator inkrementieren und Pr�fung auf Ende des Eingabestrings
		if(++it == iend) 
		{ // Kein weiteres Zeichen verf�gbar, letztes Zeichen schreiben und beenden
			out.push_back(parentPos);
			return out;
		}

		// Nur hier wird das n�chste Zeichen eingelesen
		// charToInt() aus Basisklasse wandelt auch Sonderzeichen korrekt in Integer
	    symbolVal = charToInt(*it); 

		// Hashwert berechnen (h(x) in Formeln) und Rehashing initialisieren
		hashVal = CDoubleHashing::getInstance().hash(parentPos, symbolVal, DICT_SIZE, attemptCounter.getValue());
 		pos = hashVal; // Adressen f�r das Rehashing

		// Rehashing, wenn Position bereits belegt mit falschen Werten f�r symbolVal oder parentPos
		while(m_symbolTable[pos].getSymbol() != "" &&
			  !(m_symbolTable[pos].getSymbol()[0] == symbolVal && m_symbolTable[pos].getParent() == parentPos))
		{
			// Inkrementiere Rehashing-Z�hler (i in Formeln)
			attemptCounter.count(); 
			// Neue Position pos (hi(x) in Formeln)
		    // Hashwert hashVal bleibt unver�ndert
			pos = CDoubleHashing::getInstance().hash(parentPos, symbolVal, DICT_SIZE, attemptCounter.getValue());

			// Terminierung der Endlosschleife, wenn Dictionary voll
			if(pos==hashVal) return out; // Zyklisches Hashing, Endlosschleife
		}
		// Rehashing-Z�hler zur�cksetzen f�r n�chsten Durchlauf
		attemptCounter.setValue(0);

		// Falls leere Position gefunden, dann ist Gesamtstring noch nicht im Dictionary,
		// ansonsten muss String um das n�chste Zeichen verl�ngert werden
		if(m_symbolTable[pos].getSymbol() == "")
		{
			// Letzten gefundenen String ausgeben, konkret also Position des letzten gefundenen Zeichens	
			out.push_back(parentPos);

			// Neuer Eintrag in den Dictionary f�r den verl�ngerten String
			m_symbolTable[pos].setParent(parentPos);
			m_symbolTable[pos].setSymbol(intToString(symbolVal));

			// Last-First-Property, �bernehme letztes Zeichen als erstes Zeichen des neuen Strings
			// F�r einzelnes Zeichen entspricht Position der ASCII-Zahl
			parentPos = symbolVal;
		}
		else
		{ 
			// Bisheriger String ist bereits vorhanden, letztes Zeichen wird zum Parent,
			// im n�chsten Schritt wird dann n�chstes Zeichen im Kopf der while-Schleife eingelesen
			parentPos = pos;
		}
	}

	return out;

} // Ende encode

//------------------------------------------------------------------------------------------

// Decoder f�r die Trie-Version
// Zur Zeit wird der Decoder nicht verlangt. Daher folgt erst eine Dummy-Implementierung, dann (auskommentiert) 
// die voll funktionsf�hige echte Implementierung.

string CLZWTrie::decode(const vector<unsigned int>& in)
{return "Kein Decoder implementiert!";}


//string CLZWTrie::decode(const vector<unsigned int>& in)
//{
//	// in - empfangene Indexwerte
//	// R�ckgabewert ist der zum Indexwert korrespondierende decodierte String
//	// Dictionary m_symbolTable wurde im Konstruktor bereits mit Alphabet belegt
//	
//	// out Ausgabestring
//	string out="";
//
//	// Vorheriger Ausgabestring
//	string Sv = "";
//	// Aktueller Ausgabestring
//	string S = "";
//	// Iterator �ber die Eingabewerte in vector<unsigned int>
//	vector<unsigned int>::const_iterator it;
//	// Index w�hrend Backtracking im Trie
//	int is = 0;
//
//	// parentPos: Position des Vaterknotens, im Dictionary vorhanden
//	// symbolVal: Numerischer ASCII-Wert des n�chsten Zeichens des Eingabestrings
//	unsigned int parentPos = 0;
//	unsigned int symbolVal = 0;
//	// Hashwert, bleibt w�hrend Rehashing unver�ndert
//	unsigned int hashVal = 0;
//	// Index, tempor�re Adresse w�hrend Hashing und Rehashing
//	unsigned int pos = 0;
//	// Counter f�r das Rehashing, initial 0
//	CForwardCounter attemptCounter;
//	attemptCounter.setValue(0); // Initialisierung
//
//
//	// Start der Decodierung
//	it = in.begin();
//	if(it == in.end())
//		return out; // Eingabedatenstrom leer
//	else 
//	{
//		// Ersten empfangenen Index direkt speichern
//		parentPos = *it;
//		// Ersten String einlesen und in Sv speichern
//		// Inhalt in[0] enth�lt den Index ins Array
//		// Sv erh�lt das erste Symbol
//		Sv = m_symbolTableDec[parentPos].getSymbol();
//		// parent muss -1 sein, braucht nicht eingelesen zu werden
//		// Ausgabe des ersten Strings
//		out = Sv;
//		// Jetzt muss Sv+x noch als neuer Eintrag abgespeichert werden, aber 
//		// dazu fehlt noch das erste Zeichen x des n�chsten empfangenen Strings S
//		// Die Speicheradresse ist dann mit hash(parentPos, x) zu berechnen
//	
//	// Schleife �ber Eingabevektor in, d.h. empfangene Indexwerte
//	for (it = ++in.begin(); it != in.end(); it++) {
//
//		// Sv vorheriger String
//		// S aktueller String
//		// parentPos Index des vorherigen String-Endes (von Sv)
//		// *it Index des aktuellen String-Endes (von S)
//
//		// String zum Indexwert *it ermitteln
//		if(m_symbolTableDec[*it].getParent() == -2)
//		{   // Spezialfall, Index *it noch nicht vorhanden
//			// Neuer Eintrag S ist Sv+x, wobei erstes Zeichen von Sv 
//			// als x nach hinten kopiert wird
//
//			// Symbol f�r neuen Eintrag
//			symbolVal = charToInt(Sv.front());
//			// aktueller String
//			S = Sv + Sv.front();
//			
//		} 
//		else
//		{   // Index vorhanden, String dazu nun rekonstruieren
//			// Empfangenen Index decodieren
//			// is ist lokaler Index in Loop
//			S = m_symbolTableDec[*it].getSymbol();
//			// Von parent zu parent hangeln bis zur Wurzel des Trie
//			is = m_symbolTableDec[*it].getParent();
//			while(is>=0)
//			{
//				// Neues Symbol vorn anh�ngen
//				S = m_symbolTableDec[is].getSymbol() + S;
//				is = m_symbolTableDec[is].getParent();
//			}
//			// Symbol f�r neuen Eintrag
//			symbolVal = charToInt(S.front());
//		}
//		// S enth�lt jetzt neu decodierten String
//		// Sv enth�lt den vorherigen String
//
//		// Decodierten String S an Ausgabe h�ngen
//		out = out + S;
//
//		// Zum Speichern wird nur das letzte Zeichen symbolVal und dessen Parent parentPos ben�tigt.
//		// Hashwert berechnen (h(x) in Formeln) und Rehashing initialisieren
//		
//
//		// Die Speicherung wurde aus dem Encoder �bernommen.
//		hashVal = CDoubleHashing::getInstance().hash(parentPos, symbolVal, DICT_SIZE, attemptCounter.getValue());
// 		pos = hashVal; // Adressen f�r das Rehashing
//
//		// Rehashing, wenn Position bereits belegt mit falschen Werten f�r symbolVal oder parentPos
//		while(m_symbolTableDec[pos].getSymbol() != "" &&
//			  !(m_symbolTableDec[pos].getSymbol()[0] == symbolVal && m_symbolTableDec[pos].getParent() == parentPos))
//		{
//			// Inkrementiere Rehashing-Z�hler (i in Formeln)
//			attemptCounter.count(); 
//			// Neue Position pos (hi(x) in Formeln)
//		    // Hashwert hashVal bleibt unver�ndert
//			pos = CDoubleHashing::getInstance().hash(parentPos, symbolVal, DICT_SIZE, attemptCounter.getValue());
//
//			// Terminierung der Endlosschleife, wenn Dictionary voll
//			if(pos==hashVal) return out; // Zyklisches Hashing, Endlosschleife
//		}
//		// Rehashing-Z�hler zur�cksetzen f�r n�chsten Durchlauf
//		attemptCounter.setValue(0);
//
//		// Falls leere Position gefunden, dann ist Gesamtstring noch nicht im Dictionary
//		// und wird eingetragen, sonst mache nichts.
//		if(m_symbolTableDec[pos].getSymbol() == "")
//		{
//			// Neuer Eintrag in den Dictionary f�r den verl�ngerten String
//			m_symbolTableDec[pos].setParent(parentPos);
//			m_symbolTableDec[pos].setSymbol(intToString(symbolVal));
//
//			// 
//			parentPos = *it;
//		}
//		// Ende des aus dem Encoder kopierten Teils
//		
//
//		// Decodierten String f�r n�chsten Schritt sichern
//		Sv = S;
//	}
//	return out;
//	} // Ende else nicht-leerer Datenstrom
//}
