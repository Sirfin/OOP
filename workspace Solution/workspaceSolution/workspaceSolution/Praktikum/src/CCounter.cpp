#include "CCounter.h"

CCounter::CCounter() : m_value(0) {
}

int CCounter::getValue() const {
	return m_value;
}

void CCounter::setValue(int value) {
	m_value = value;
}

void CCounter::count() {
}

bool CCounter::operator<(const CCounter& other) const {
	return m_value < other.m_value;
}

bool CCounter::operator>(const CCounter& other) const {
	return m_value > other.m_value;
}