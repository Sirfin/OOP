#include "CLZWArray.h"
#include "iostream"

CLZWArray::CLZWArray()
{	
	// Initialisierung der ersten 256 Eintr�ge im Dictionary mit den 
	// entsprechenden Zeichen der Ascii-Tabelle
	for (int i = 0; i < 256; i++) {
		// setSymbol erwartet String
		m_symbolTable[i].setSymbol(intToString(i));
	}
}

vector<unsigned int> CLZWArray::encode(const std::string &in)
{
	// Output des Algorithmus
	vector<unsigned int> out;
	// S: Zeichenkette, die im Dictionary vorhanden ist
	// Sx: Um x erweiterte Zeichenkette S, die im Dictionary gesucht wird
	string S, Sx;
	// n�chste freie Stelle im Dictionary
	int nextDictPos = 256;

	// Abbrechen, wenn Zeichenkette leer
	if(in.begin() == in.end()) return  out;
	
	// �ber den Eingabestring iterieren
	for (string::const_iterator it = in.begin(); it != in.end(); it++) {
		Sx = S + *it;
		// Ix im Dictionary suchen
		int pos = searchInTable(Sx);
		
		// pos < 0 wenn Ix nicht im Dictionary
		if (pos < 0) {
			// Dictionaryindex von S ausgeben
			out.push_back(searchInTable(S));			
			// Sx im Dictionary speichern
			m_symbolTable[nextDictPos].setSymbol(Sx);
			nextDictPos++;
			// Neues S ist nur das aktuelle letzte Zeichen x 
			S = *it;
		} else {
			// Sx im Dictionary vorhanden
			S = Sx;	
		}
	}
	
	// letzte Zeichenkette ausgeben
	out.push_back(searchInTable(S));

	return out;
}

// decodiert (restauriert) den String in mit Hilfe des LZW-Algorithmus
string CLZWArray::decode(const vector<unsigned int> &in)
{
	// in empfangene Indexwerte
	// R�ckgabewert ist der zum Indexwert korrespondierende decodierte String
	
	// out Ausgabestring
	string out="";
	// n�chste freie Stelle im Dictionary
	// Dictionary m_symbolTable wurde im Konstruktor bereits mit Alphabet belegt
	unsigned int nextDictPos = 256;
	// Vorheriger Ausgabestring
	string Sv = "";
	// Aktueller Ausgabestring
	string S = "";
	// Iterator �ber die Eingabewerte in vector<unsigned int>
	vector<unsigned int>::const_iterator it;

	// Start der Decodierung
	it = in.begin();
	if(it == in.end())
		return ""; // Eingabedatenstrom leer
	else 
	{
		// Ersten String einlesen und in Sv speichern
		// Inhalt in[0] enth�lt den Index ins Array
		Sv = m_symbolTable[*it].getSymbol();
		// Ausgabe des ersten Strings
		out = Sv;
	
	// Schleife �ber Eingabevektor in, d.h. empfangene Indexwerte
	for (it = ++in.begin(); it != in.end(); it++) {
		// String zum Indexwert *it ermitteln
		if(*it==nextDictPos)
		{   // Spezialfall, Index noch nicht gespeichert
			// Neuer Eintrag ist Sv, wobei erstes Zeichen 
			// nach hinten kopiert wird
			S = Sv + Sv[0];//S = Sv + Sv.front();//std::string::front() nur bei C++11
		} 
		else
		{
			// Empfangenen Index decodieren
			S = m_symbolTable[*it].getSymbol();
		}
			// Decodierten String an Ausgabe h�ngen
			out = out + S;
			// Neuer Eintrag ist vorherige Ausgabe plus
			// erstes Zeichen des neuen Strings
			m_symbolTable[nextDictPos].setSymbol( Sv + S[0] );//m_symbolTable[nextDictPos].setSymbol( Sv + S.front() ); // std::string::front() nur bei C++11
			nextDictPos++;
			// Decodierten String f�r n�chsten Schritt sichern
			Sv = S;
	}
	return out;
	} // Ende else nicht-leerer Datenstrom
};

int CLZWArray::searchInTable(const string &str)
{
	for (int i = 0; i < DICT_SIZE; i++) {
		if (m_symbolTable[i].getSymbol() == str) {
			return i;
		}
	}

	// wenn str nicht im Dictionary gefunden wurde, wird -1 zur�ckgegeben
	return -1;
}
