#include "XOutOfBounds.h"

XOutOfBounds::XOutOfBounds(const char* msg) : msg(msg) {
}

XOutOfBounds::~XOutOfBounds() throw(){
}

const char* XOutOfBounds::what() const throw(){
	return msg.c_str();
}
