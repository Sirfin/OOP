#include "CKnot.h"

// parent mit -2(unused) initialisiert
CKnot::CKnot() : m_parent(-2) {
}

int CKnot::getParent() const {
	return m_parent;
}

void CKnot::setParent(int parent) {
	m_parent = parent;
}