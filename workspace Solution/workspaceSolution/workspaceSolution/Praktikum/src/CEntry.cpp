#include "CEntry.h"

// Zum Laufzeitbeginn wird die Anzahl aller Instanzen auf 0 gesetzt
unsigned int CEntry::m_number = 0;

CEntry::CEntry() {
	// Erzeugen einer Instanz f�hrt zur Inkrementierung von anzahl
	m_number++;
}

CEntry::~CEntry() {
	// L�schen einer Instanz f�hrt zur Dekrementierung von anzahl
	m_number--;
}

const string& CEntry::getSymbol() const {
	return m_symbol;
}

void CEntry::setSymbol(string symbol) {
	m_symbol = symbol;
}

unsigned int CEntry::getNumber() {
	return m_number;
}