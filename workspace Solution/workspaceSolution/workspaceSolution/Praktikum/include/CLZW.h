/***************************************************
*                       CLZW                       *
****************************************************
* Entwurfsmuster: Strategie                        *
* Diese Klasse repr�sentiert innerhalb des         *
* Entwurfsmusters Strategie die eigentliche        *
* Strategie. Von ihr werden konkrete Strategien    *
* abgeleitet. Von der Klasse selbst k�nnen keine   *
* Instanzen erstellt werden, sie ist abstrakt.     *
* Hier dient die Strategie dazu, zur Laufzeit      *
* zwischen einer Listen- und einer Trie-           *
* Implementierung des LZW-Algorithmus zu w�hlen.   *
****************************************************/
#pragma once

#include <string>
#include <vector>

// Gr��e des Dictionary (16 bit)
//#define DICT_SIZE 65536
#define DICT_SIZE 2000  // Kleinere Gr��e f�r k�rzere Rechenzeit

using namespace std;

class CLZW
{
public:
	// Destruktor
	virtual ~CLZW();
	// encodiert (komprimiert) den String in mit Hilfe des LZW-Algorithmus
	virtual vector<unsigned int> encode(const string &) = 0;
	// decodiert (restauriert) den String in mit Hilfe des LZW-Algorithmus
	virtual string decode(const vector<unsigned int> &) = 0;
	// erm�glicht das Umwandeln von Integern zu string
	static string intToString(int i);
	// erm�glicht das Umwandeln von einzelnen Elementen eines string in 
	// die zugeh�rige ASCII-Zahl auch f�r Eintr�ge 128-255 (z.B. Umlaute)
	static unsigned int charToInt(char);
};