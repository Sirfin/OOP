/****************************************************
*                     CLZWArray                     *
*****************************************************
* Entwurfsmuster: Strategie                         *
* Diese Klasse repräsentiert innerhalb des          *
* Entwurfsmusters Strategie eine konkrete Strategie.*
* Diese Klasse stellt die Arrayimplementierung dar. *
*****************************************************/
#pragma once

#include "CLZW.h"
#include "CArray.h"
#include "CEntry.h"

class CLZWArray : public CLZW
{
public:
	// Konstruktor
	CLZWArray();
	// encodiert (komprimiert) den String in mit Hilfe des LZW-Algorithmus
	vector<unsigned int> encode(const string &in);
	// decodiert (restauriert) den String in mit Hilfe des LZW-Algorithmus
	string decode(const vector<unsigned int> &in);

private:
	// sucht Einträge mit dem Inhalt str im Dictionary
	int searchInTable(const string &str);
	
	// Dictionary (Liste mit den Symbolen)
	CArray<CEntry, DICT_SIZE> m_symbolTable;
};