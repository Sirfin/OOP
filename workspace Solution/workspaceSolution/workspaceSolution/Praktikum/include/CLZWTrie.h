/****************************************************
*                      CLZWTrie                     *
*****************************************************
* Entwurfsmuster: Strategie                         *
* Diese Klasse repräsentiert innerhalb des          *
* Entwurfsmusters Strategie eine konkrete Strategie.*
* Diese Klasse stellt die Trie-Implementation dar.  *
*****************************************************/
#pragma once

#include "CLZW.h"
#include "CArray.h"
#include "CKnot.h"
#include "CDoubleHashing.h"
#include "CForwardCounter.h"

class CLZWTrie : public CLZW
{
public:
	// Konstruktor
	CLZWTrie();
	// encodiert (komprimiert) den String in mit Hilfe des LZW-Algorithmus
	vector<unsigned int> encode(const string &in);
	// decodiert (restauriert) den String in mit Hilfe des LZW-Algorithmus
	string decode(const vector<unsigned int> &);

private:
	// Dictionary Encoder (Liste mit den Symbolen)
	CArray<CKnot, DICT_SIZE> m_symbolTable;
	// Dictionary Decoder
	CArray<CKnot, DICT_SIZE> m_symbolTableDec;
};