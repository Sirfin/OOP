/****************************************************
*                       CKnot                       *
*****************************************************
* Diese Klasse repr�sentiert einen Knoten in der    *
* Trie-Implementierung des LZW.                     *
*****************************************************/
#pragma once

#include "CEntry.h"

class CKnot : public CEntry
{
public:
	// Konstruktor, parameterlos
	CKnot();

	// Getter f�r das Attribut parent
	int getParent() const;
	// Setter f�r das Attribut parent
	void setParent(int parent);

private:
	// Vaterknoten
	int m_parent;
};