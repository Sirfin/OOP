#pragma once

class CCounter
{
public:
	CCounter();
	int getValue() const;
	void setValue(int value);
	virtual void count();
	bool operator<(const CCounter& other) const;
	bool operator>(const CCounter& other) const;

private:
	int m_value;
};