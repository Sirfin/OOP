/****************************************************
*                      CEntry                       *
*****************************************************
* Diese Klasse repr�sentiert einen Listeneintrag in *
* der Listen-Implementierung des LZW. Gleichzeitig  *
* dient sie als Basisklasse f�r die Knoten in der   *
* Trie-Implementierung.                             *
*****************************************************/
#pragma once

#include <string>

using namespace std;

class CEntry
{
public:
	// Konstruktor, parameterlos
	CEntry();
	// Destruktor
	~CEntry();

	// Getter f�r das Attribut symbol
	const string& getSymbol() const;
	// Setter f�r das Attribut symbol
	void setSymbol(string symbol);

	// Gibt die Anzahl aller Instanzen der Klasse zur�ck
	static unsigned int getNumber();

private:
	// Der "eigentliche" Inhalt des Eintrags
	string m_symbol;
	// Die Anzahl aller Instanzen der Klasse
	static unsigned int m_number;
};