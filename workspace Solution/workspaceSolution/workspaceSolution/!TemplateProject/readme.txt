This is a template project with all include/library paths setup for use with the Google-Test library. 
When creating a new project:
 1. make copy of this template project directory, rename it as needed
 2. in eclipse: "File/Import/General/Existing Projects into Workspace", choose the directory you just created
 3. rename the project as needed within eclipse
 4. optional: if you just wiped an exisitng project with existing headers and sources, just copy them to the "include" resp. "src" subdirectories, but make sure there is a "main.cpp"