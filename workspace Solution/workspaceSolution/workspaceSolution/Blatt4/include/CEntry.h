/****************************************************
*                      CEntry                       *
*****************************************************
* Diese Klasse repr�sentiert einen Listeneintrag in *
* der Listen-Implementierung des LZW. Gleichzeitig  *
* dient sie als Basisklasse f�r die Knoten in der   *
* Trie-Implementierung.                             *
*****************************************************/
#pragma once

#include <string>

using namespace std;

class CEntry
{
public:
	// Konstruktor, parameterlos
	CEntry();
	// Destruktor
	virtual ~CEntry();
	
	// Getter f�r das Attribut symbol
	string& getSymbol();
	// Setter f�r das Attribut symbol
	void setSymbol(string str);
	
	// Gibt die Anzahl aller Instanzen der Klasse zur�ck
	static unsigned int getAnzahl();

private:
	// Der "eigentliche" Inhalt des Eintrags
	string symbol;
	// Die Anzahl aller Instanzen der Klasse
	static unsigned int anzahl;
};