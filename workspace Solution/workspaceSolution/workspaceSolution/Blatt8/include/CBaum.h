/***************************************************************************
cbaum.h  -  description
-------------------
begin                : 01.10.2002
copyright            : (C) 2002-2005 by TUHH, Vision Systems
email                : kricke@tu-harburg.de
origin               : Informatik fuer Ingenieure II
***************************************************************************/



#ifndef CBAUM_H
#define CBAUM_H

#include <iostream>
#include <cstddef>

template <class T> class CKnoten;

template <class T>
class CBaum
{
public:
	CBaum();
	~CBaum();
	void insert(T value);
	void inorder(std::ostream& ost);
	void preorder(std::ostream& ost);
	void postorder(std::ostream& ost);
private:
	CKnoten<T>* m_wurzel;
	void deleteBaum(CKnoten<T>* knoten);
	void insert(T value, CKnoten<T>* knoten);
	void inorder(std::ostream& ost, CKnoten<T>* knoten);
	void preorder(std::ostream& ost, CKnoten<T>* knoten);
	void postorder(std::ostream& ost, CKnoten<T>* knoten);

};

template <class T>
CBaum<T>::CBaum():
m_wurzel(NULL)
{
}

template <class T>
CBaum<T>::~CBaum()
{
	deleteBaum(m_wurzel);
}

template <class T>
void CBaum<T>::insert(T value)
{
	insert(value, m_wurzel);
}

template <class T>
void CBaum<T>::insert(T value, CKnoten<T>* knoten)
{
	if (knoten == NULL){
		m_wurzel = new CKnoten<T>(value);
	}
	else
	{
		if (value < knoten->m_value)
		{
			if (knoten->m_links == NULL)
			{
				knoten->m_links = new CKnoten<T>(value);
			}
			else
			{
				insert(value, knoten->m_links);
			}
		}
		else
		{
			if (knoten->m_rechts == NULL)
			{
				knoten->m_rechts = new CKnoten<T>(value);
			}
			else
			{
				insert(value, knoten->m_rechts);
			}
		}
	}
}

template <class T>
void CBaum<T>::inorder(std::ostream& ost)
{
	inorder(ost, m_wurzel);
}

template <class T>
void CBaum<T>::inorder(std::ostream& ost, CKnoten<T>* knoten)
{
	if (knoten != NULL)
	{
		inorder(ost, knoten->m_links);
		ost << knoten->m_value /*<< " "*/;
		inorder(ost, knoten->m_rechts);
	}
}


template <class T>
void CBaum<T>::preorder(std::ostream& ost)
{
	preorder(ost, m_wurzel);
}

template <class T>
void CBaum<T>::preorder(std::ostream& ost, CKnoten<T>* knoten)
{
	if (knoten != NULL)
	{
		ost << knoten->m_value /*<< " "*/;
		preorder(ost, knoten->m_links);
		preorder(ost, knoten->m_rechts);
	}
}


template <class T>
void CBaum<T>::postorder(std::ostream& ost)
{
	postorder(ost, m_wurzel);
}

template <class T>
void CBaum<T>::postorder(std::ostream& ost, CKnoten<T>* knoten)
{
	if (knoten != NULL)
	{
		postorder(ost, knoten->m_links);
		postorder(ost, knoten->m_rechts);
		ost << knoten->m_value /*<< " "*/;
	}
}

template <class T>
void CBaum<T>::deleteBaum(CKnoten<T>* knoten)
{
	if (knoten != NULL)
	{
		deleteBaum(knoten -> m_links);
		deleteBaum(knoten -> m_rechts);
		delete knoten;
		knoten = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

template <class T>
class CKnoten
{
	friend class CBaum<T>;
private:
	CKnoten(T& value);
	~CKnoten();

	CKnoten* m_links;
	CKnoten* m_rechts;
	T m_value;

};

template <class T>
CKnoten<T>::CKnoten(T& value):
m_value(value),
m_links(NULL),
m_rechts(NULL)
{
}

template <class T>
CKnoten<T>::~CKnoten()
{
}


#endif // CBAUM_H

