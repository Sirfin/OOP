#pragma once

class CCounter
{
public:
	CCounter();
	virtual ~CCounter();
	int getValue();
	void setValue(int newValue);
	virtual void count();
private:
	int actValue; 
};



