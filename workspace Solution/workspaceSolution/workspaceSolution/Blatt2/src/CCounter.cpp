#include "CCounter.h"

CCounter::CCounter(): actValue(0) {
}

CCounter::~CCounter() {
}

int CCounter::getValue() {
	return actValue;
}

void CCounter::setValue(int newValue) {
	actValue = newValue;
}

void CCounter::count() {
	actValue++;
} 
