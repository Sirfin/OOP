REM Delete the code index, will be recreated on next start of eclipse.
del %cd%\.metadata\.plugins\org.eclipse.cdt.core\*.pdom

REM Delete the resource history files.
del /s /q %cd%\.metadata\.plugins\org.eclipse.core.resources\.history\*.*
FOR /D %%p IN ("%cd%\.metadata\.plugins\org.eclipse.core.resources\.history\*.*") DO rmdir "%%p" /s /q

REM Delete various index files.
del /s /q %cd%\.metadata\*.index
del %cd%\.metadata\.plugins\org.eclipse.jdt.core\savedIndexNames.txt

del /s /q %cd%\RemoteSystemsTempFiles\*.*
rmdir /s /q %cd%\RemoteSystemsTempFiles

REM Delete various log files.
del /s /q %cd%\.metadata\*.log

pause