#pragma once
#include "CCounter.h"

class CVariableCounter :
	public CCounter
{
public:
	CVariableCounter();
	CVariableCounter(int param_stepSize);
	void count();
private:
	int stepSize;
};

