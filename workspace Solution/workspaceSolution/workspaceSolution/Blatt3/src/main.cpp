/************************************************************
 *                  Unit-Test f�r Blatt 3                   *
 ************************************************************/

// Bitte hier die aktuelle Aufgabe festlegen
#define AUFGABE_1

// Ben�tigte Bibliotheken einbinden
#include <iostream>

// Google Test einbinden
#include "gtest/gtest.h"

#include "CCounter.h"
#include "CForwardCounter.h"
#include "CVariableCounter.h"

// Grundfunktionalit�t der Z�hler testen
#ifdef AUFGABE_1
TEST(TypeTest, CCounter) {
	CCounter counter;
	EXPECT_EQ(0, counter.getValue()) << "Wert von CCounter ist initial nicht 0!";
	int value = counter.getValue() + 10;
	counter.setValue(value);
	EXPECT_EQ(value, counter.getValue()) << "Wert von CCounter kann nicht gesetzt werden!";
	counter.count();
	EXPECT_EQ(value, counter.getValue()) << "CCounter zaehlt faelschlicherweise!";
}

TEST(TypeTest, CForwardCounter) {
	CForwardCounter counter;
	EXPECT_EQ(0, counter.getValue()) << "Wert von CForwardCounter ist initial nicht 0!";
	int value = counter.getValue() + 10;
	counter.setValue(value);
	EXPECT_EQ(value, counter.getValue()) << "Wert von CForwardCounter kann nicht gesetzt werden!";
	counter.count();
	EXPECT_EQ(value + 1, counter.getValue()) << "CForwardCounter zaehlt falsch!";
}

TEST(TypeTest, CVariableCounter) {
	CVariableCounter counter(5);
	EXPECT_EQ(0, counter.getValue()) << "Wert von CVariableCounter ist initial nicht 0!";
	int value = counter.getValue() + 10;
	counter.setValue(value);
	EXPECT_EQ(value, counter.getValue()) << "Wert von CVariableCounter kann nicht gesetzt werden!";
	counter.count();
	EXPECT_EQ(value + 5, counter.getValue()) << "CVariableCounter zaehlt falsch!";
}

// Jetzt kommen die Polymorphietests
TEST(PolymorphieTest, CForwardCounter) {
	CCounter *counter = new CForwardCounter;
	int value = counter->getValue();
	counter->count();
	EXPECT_EQ(value + 1, counter->getValue()) << "Polymorhpie mit CForwardCounter klappt nicht!";
	delete counter;
}

TEST(PolymorphieTest, CVariableCounter) {
	CCounter *counter = new CVariableCounter(5);
	int value = counter->getValue();
	counter->count();
	EXPECT_EQ(value + 5, counter->getValue()) << "Polymorphie mit CVariableCounter klappt nicht!";
}
#endif

// Es folgen die Tests f�r die zweite Aufgabe
#ifdef AUFGABE_2
TEST(OperatorTest, LessThan) {
	CCounter counter1;
	CCounter counter2;
	counter1.setValue(10);
	counter2.setValue(20);
	EXPECT_LT(counter1, counter2) << "Operator fuer kleiner als funktioniert nicht!";
	EXPECT_FALSE(counter2 < counter1) << "Operator fuer kleiner als funktioniert nicht!";
}

TEST(OperatorTest, GreaterThan) {
	CCounter counter1;
	CCounter counter2;
	counter1.setValue(10);
	counter2.setValue(20);
	EXPECT_GT(counter2, counter1) << "Operator fuer groesser als funktioniert nicht!";
	EXPECT_FALSE(counter1 > counter2) << "Operator fuer groesser als funktioniert nicht!";
}
#endif

// Hauptprogramm
#ifndef AUFGABE_2b
int main(int argc, char** argv) {
	// Google Test initialisieren
	testing::InitGoogleTest(&argc, argv);

	// alle Tests ausf�hren
	int ret = RUN_ALL_TESTS();

	// und Ergebnis anzeigen und zur�ckgeben
	std::cin.get();
	return ret;
}
#endif
