#include "CForwardCounter.h"

CForwardCounter::CForwardCounter() : CCounter() 
{
}

void CForwardCounter::count() 
{
	int tmp_value = getValue();
	setValue(tmp_value + 1);
}
