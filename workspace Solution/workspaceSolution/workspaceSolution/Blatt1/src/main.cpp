/************************************************************
 *                  Main f�r Blatt 1                   		*
 ************************************************************/

#include <iostream>

using namespace std;

void printHelloWorld(int numberOfPrints) {
			
	for(int i=0; i<numberOfPrints; i++)
		cout << "!!!Hello World!!!" << endl;
		
}	

int main() {
	
	int   numberOfPrints = 1;
	int* pNumberOfPrints = &numberOfPrints;
	
	(*pNumberOfPrints)++;
	
	printHelloWorld(numberOfPrints);
		
	return 0;
}
