REM Script to remove all solution related files (and this script itself) to create a workspace ready for distribution.

cd %~dp0

del /s CCounter.h
del /s CCounter.cpp
del /s CForwardCounter.h
del /s CForwardCounter.cpp
del /s CVariableCounter.h
del /s CVariableCounter.cpp

del /s CEntry.h
del /s CEntry.cpp
del /s CKnot.h
del /s CKnot.cpp

del /s CArray.h
del /s XOutOfBounds.h
del /s XOutOfBounds.cpp

del /s CDoubleHashing.h
del /s CDoubleHashing.cpp

del /s CBaum_a.h
del /s CBaum.h

del /s CLZW.h
del /s CLZWArray.h
del /s CLZWTrie.h
del /s CLZW.cpp
del /s CLZWArray.cpp
del /s CLZWTrie.cpp

del /s Counter.java
del /s BaseCounter.java
del /s ForwardCounter.java
del /s VariableCounter.java

pause
