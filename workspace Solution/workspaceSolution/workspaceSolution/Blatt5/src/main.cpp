/************************************************************
 *                  Unit-Test f�r Blatt 5                   *
 ************************************************************/

// Bitte hier die aktuelle Aufgabe festlegen
#define AUFGABE_3

// Ben�tigte Bibliotheken einbinden
#include <iostream>

// Google Test einbinden
#include "gtest/gtest.h"

#include "CEntry.h"

// Testen, ob das Symbol richtig gesetzt wird
TEST(CEntryTest, Symbol) {
	CEntry entry;
	entry.setSymbol("Test");
	EXPECT_EQ("Test", entry.getSymbol());
}

// Und testen, ob die Anzahl der Instanzen richtig z�hlt
TEST(CEntryTest, CountInstances) {
	EXPECT_EQ(0, CEntry::getNumber());
	CEntry* entries[10];
	for (int i = 0; i < 10; i++) {
		entries[i] = new CEntry;
		EXPECT_EQ(i+1, CEntry::getNumber());
	}
	for (int i = 0; i < 10; i++) {
		delete entries[i];
		EXPECT_EQ(9-i, CEntry::getNumber());
	}
}

// Hauptprogramm
int main(int argc, char** argv) {
	// Google Test initialisieren
	testing::InitGoogleTest(&argc, argv);

	// alle Tests ausf�hren
	int ret = RUN_ALL_TESTS();

	// und Ergebnis anzeigen und zur�ckgeben
	std::cin.get();
	return ret;
}
